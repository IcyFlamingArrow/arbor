# Copyright 2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user='BLAKE2' tag=v${PV} ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
require providers

export_exlib_phases src_configure

SUMMARY="C library providing BLAKE2b, BLAKE2s, BLAKE2bp and BLAKE2sp"

LICENCES="CC0"
SLOT="0"
MYOPTIONS="
    openmp [[ description = [ Build with OpenMP support via libgomp ] ]]
"

DEPENDENCIES="
    build+run:
        openmp? ( sys-libs/libgomp:= )
"

DEFAULT_SRC_CONFIGURE_PARAMS=( --disable-native )

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( openmp )

libb2_src_configure() {
    # clang can't use libgomp for OpenMP
    if optionq openmp; then
        providers_set 'cc gcc'
        providers_set 'c++ gcc'
        providers_set 'cpp gcc'
    fi

    default
}

