# Copyright 2009-2011 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'libaio-0.3.107.ebuild' from Gentoo, which is:
#     Copyright 1999-2009 Gentoo Foundation

require pagure

SUMMARY="Asynchronous input/output library that uses the kernels native interface"
DESCRIPTION="
AIO enables even a single application thread to overlap I/O operations with other
processing, by providing an interface for submitting one or more I/O requests in
one system call (io_submit()) without waiting for completion, and a separate interface
(io_getevents()) to reap completed I/O operations associated with a given completion
group. Support for kernel AIO has been included in the 2.6 Linux kernel.
"
HOMEPAGE="${HOMEPAGE} http://lse.sourceforge.net/io/aio.html"

LICENCES="LGPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS=""

# Full tests (check) want to mount things, need root. Instead we only run partcheck.
#RESTRICT="test userpriv"

DEPENDENCIES="
    test:
        dev-tcl/expect
"

DEFAULT_SRC_INSTALL_PARAMS=( prefix="/usr/$(exhost --target)" )

src_prepare() {
    default

    # Remove -Werror to make it work with gcc8 for example
    edo sed -e "s: -Werror::" -i harness/Makefile
}

src_test() {
    emake -j1 partcheck

#    esandbox allow /dev/loop
#    edo pushd "${WORK}"/harness >/dev/null
#    edo mkdir testdir
#    emake -j1 check prefix="${WORK}/src" libdir="${WORK}/src"
#    edo popd >/dev/null
}

src_install() {
    default

    doman man/*
}

