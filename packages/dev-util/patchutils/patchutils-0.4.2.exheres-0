# Copyright 2009-2015 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ] \
        github [ user=twaugh ]

SUMMARY="A small collection of programs that operate on patch files"
DESCRIPTION="
Patchutils contains a collection of tools for manipulating patch files: interdiff,
combinediff, flipdiff, filterdiff, fixcvsdiff, rediff, lsdiff, grepdiff, splitdiff,
recountdiff, and unwrapdiff. You can use interdiff to create an incremental patch
between two patches that are against a common source tree, combinediff for creating
a cumulative diff from two incremental patches, and flipdiff to transpose two
incremental patches. Filterdiff is for extracting or excluding patches from a
patch set based on modified files matching shell wildcards. Lsdiff lists modified
files in a patch. Rediff, recountdiff, and unwrapdiff correct hand-edited (or
otherwise broken) patches.
"

REMOTE_IDS="freecode:${PN}"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="pcre"
DEPENDENCIES="
    pcre? ( dev-libs/pcre2 )
"

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'pcre pcre2'
)

